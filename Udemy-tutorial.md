# start minikube
minikube start
cat ~/.kube/config

minikube dashboard
kubectl run hello-kubernetes --image=k8s.grc.io/echoserver:1.4 
kubectl expose deployment hello-kubernetes --type=NodePort --port=8080
minikube service hello-kubernetes
minikube stop
minikube delete


kubectl get nodes
kebuctl config get-contexts
kebuctl config get-contexts kind-kind-2 

# minikube vs docker client vs kops vs kubeadm
- there are multiple tool to install a kubernetes cluster
- i showed you to install minikube / docker client to do local install
- if you want a production cluster, you need difeferent tooling
  - minikube and docker client are great for local setups, but not for real clusters
  - Kops and kubeadm are tooling to spin up a production cluster
  - you don't need both tools, just one of them
- on aws, the best tool is kops
    - at same point aws EKS(hosted kubernetes) will be available, at that point this will probably be the preferred option
- for other install, or if you can't get kops work, you can use kubeadm
    - kubeadm is an alternative approach, kps still recommended on aws - you alse have aws integration with kops automatically
    - the kubeadm lectures can be found at the end of this course

# KOPS setting up kubernetes on AWS
Cloud setup
- to setup kubernetes on aws, you can use a tool called kops
    - kops stand for kubernetes Operation
- The tool allows yiu to do production grade kubernetes installations, upgrades and management
    - i will use this tool to start a kubernetes cluster on AWS
- there also a legacy tool called kube-up.sh
    - this is simple tool bring up a cluster, but is now deprecated it doasn't create a production ready environment
- kops only work on unix

- Kop install
  - vagran use
    mkdir ubuntu
    - vagrant init ubuntu/xenial64
    - vagrant up
    - vagrant ssh-config
    - vagrant ssh
    - puttygen putty
  - prepare
    - https://github.com/kubernetes/kops
    - wget https://github.com/kubernetes/kops/releases/download/v1.25.2/kops-linux-amd64
    - sudo mv kops-linux-amd64 /usr/local/bin/
    - sudo apt install python-pip
    - sudo pip install awscli
    - aws
    - create free tier aws
    - create iam user in search menu and give access 
        AmazonEC2FullAccess
        AmazonRoute53FullAccess
        AmazonS3FullAccess
        IAMFullAccess
        AmazonVPCFullAccess

    - aws configure
    User name	Password	Access key ID	Secret access key	Console login link
    kops		AKIAXSFZK4SJM3O2HHNL	D5FI4wVfAbaZu+e866CiTNUqlo/gv+ZsYs7S1Jyu	https://520079729810.signin.aws.amazon.com/console
    - ls -ahl ~/.aws/
    - go to welcome screen and select s3
    - create bucket
    - entername kops-id => unique
    - or api aws s3api create-bucket --bucket kubernetes-aws-io
    - aws s3api put-bucket-versioning --bucket kubernetes-aws-io --versioning-configuration Status=Enabled
    - export KOPS_STATE_STORE=s3://kubernetes-aws-io
    - search route53
    - DNS Management
    - enter kubernetes.stackbin.tech
    - register NS to DNS Management site
    ns-1168.awsdns-18.org.
    ns-1711.awsdns-21.co.uk.
    ns-617.awsdns-13.net.
    ns-481.awsdns-60.com.
    - sudo apt install bind9-host
    - host -t NS kubernetes.stackbin.tech
    - wget https://storage.googleapis.com/kubernetes-release/v1.4.3/bin/linux/amd64/kubectl
    - or curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/darwin/amd64/kubectl
    - sudo mv kubectl /usr/local/bin/
    - sudo chmod +x /usr/local/bin/kubectl
    - ssh-keygen -f .ssh/id_rsa
    - cat .cat/id_rsa.pub
    - sudo mv /usr/local/bin/kops-linux-amd64 /usr/local/bin/kops
    - kops create cluster --name=kubernetes.stackbin.tech --state=s3://kops-id --zones=ap-southeast-1 --node-count=2 --node-size=t3.micro --master-size=t3.micro --dns-zone=kubernetes.stackbin.tech
    hint: newer kops version dont accept t2.micro anymore. you can use the newer generation of micro instances: t3.micro
    in case you need more cpu/memopy, you can use t3.small / t3.medium / t3.large / etc
    see https://ec2instances.info for an overview of types
    
    - kops create cluster \
        --name cluster.kubernetes-aws.io \
        --zones us-west-2a \
        --state s3://kubernetes-aws-io \
        --yes
    - kops validate cluster --state=s3://kubernetes-aws-io
    -  sudo kops validate cluster --name kubernetes.stackbin.tech --state s3://kops-id
    - kops edit cluster kubernetes.stackbin.tech --state=s3://kops-id => ctrlx
    - kops update cluster kubernetes.stackbin.tech --yes --state=s3://kops-id --admin
    - cat ~/.kube/config
    - kubectl get nodes
    - kubectl run hello-minikube --image=k8s.grc.io/echoserver:1.4
    - kubectl expose deployment hello-minikube --type=NodePort --port=8080
    - kubectl get services
    - to aws select EC2
    - select running intances
    - select auto schaling group
    - to security group click on name and inbound > edit > add rule > custom tcp > portrange 30032 source MyIp 
    - to instance select node in description copy ip public and enter to url browser
    - kops delete cluster kubenetes.stackbin.tech --state=s3://kops-id --yes
# Building Containers
 - git clone https://github.com/wardviaene/docker-demo
 - cd docker-demo 
 - docker build .
 - docker run -p 3000:3000 -it eb11
 
# Docker Image registry
- docker login
- docker tag imageid your-login/docker-demo
- docker push your-login/docker-demo
- docker build -t your-login/docker-demo
- docker push your-login/docker-demo

- docker login registry.gitlab.com
- docker build -t registry.gitlab.com/docker-project-list/kubernetes .
- docker push registry.gitlab.com/docker-project-list/kubernetes

# running first app on kubernetes
- kubectl create -f first-app/helloworld.yml
- kubectl get pod
- kubectl describe pod <pod>
- kubectl port-forward nodehelloworld.example.com 8081:3000
- kubectl expose pod nodehelloworld.example.com --type=NodePort --name=nodehelloworld-service
- minikube service nodehelloworld-service --url
- kubectl get service
- kubectl attach nodehelloworld-service
- kubectl exec nodehelloworld.example.com -- touch /app/test.txt
- kubectl exec nodehelloworld.example.com -- ls /app
- kubectl get service
- kubectl run -i --tty busybox --image=busybox --restart=Never -- sh
- telnet 172.17.0.5 3000
- GET /
- kubectl logs nodehelloworld.example.com

# Load Balancer
- kubectl create -f first-app/helloworld.yml
- kubectl create -f first-app/helloworld-service.yml
- aws iam create-service-linked-role --aws-service-name "elasticloadbalancing.amazonaws.com"
to see
- ECS2 > loadbalancer

# Kubernetes Basic
- Node Architecture
  - screencapture-brismart-udemy-course-learn-devops-the-complete-kubernetes-course-learn-lecture-6058574-2022-11-13-14_35_56.png
- Replication Container / scalling pods
  - kubectl create -f replication-controller/helloworld-repl-controller.yml
  - kubectl get pods
  - kubectl scale --replicas=4 -f replication-controller/helloworld-repl-controller.yml

- Deployments
  - Usefull COmmand
    - kubectl create -f deployment/helloworld.yml
    - kubectl get deployments get information on current deployments
    - kubectl get rs get information about the replica sets
    - kubectl get pods --show-labels get pods and also show labels attached to those pods
    - kubectl rollout status deployment/helloworld-deployment get deployment status
    - kubectl set image deployment/helloworld-deployment k8s-demo=k8s-demo2 run k8s-demo with image label version 2
    - kubectl edit deployment/helloworld-deployment edit deployment object
    - kubectl rollout status deployment/helloworld-deployment get status of tollout
    - kubectl rollout history deployment/helloworld-deployment get rollout history
    - kubectl rollout undo deployment/helloworld-deployment rollback to prev version
    - kubectl rollout undo deployment/helloworld-deployment --to-revision=n rollback to any version
- Services
    - kubectl create -f first-app/helloworld-nodeport-service.yml
    - minikube service helloworld-service --url
    - curl ip:port
    - kubectl describe helloworld-service
    - kubectl get svc
- Labels
    - kubectl label nodes node1 hardware=high-spec
    - kubectl label nodes node1 hardware=low-spec
    - or with yml
    ```
    deployment/helloworld-nodeselector.yml
    ```
    - kubectl get nodes --show-labels
    - kubectl get deployments
    - kubectl get pods
    - kubectl describe pod  helloworld-nodeselector
- healtcheck
    - kubectl create -f deployment/helloworld-healthcheck.yml
    - kubectl get pods
    - kubectl describe helloworld-deployment
    - 
- Readines Probe
    - kubectl create -f deployment/helloworld-liveness-readiness.yml && watch -n1 kubectl get pods
    - 
- Pod state
- Pod Lifecycle
- Secret
- WebUI

# Advance Topic
- Service Discovery
- Config Map
- Ingess Controlle
- External DNS
- 
# Useful Command
- kubectl get pod
- kubectl describe pod <pod>
- kubectl expose pod <pod> --port=444 --name=frontend
- kubectl port-forward <pod> 8080
- kubectl attach <podname> -i
- kubectl exec <pod> -- command
- kubectl label pods mylabel=awesome
- kubectl run -i --tty busybox --image=busybox --restart=Never --sh
- kubectl config view

# Error
- The connection to the server localhost:8080 was refused - did you specify the right host or port?
solution: export KUBECONFIG=/etc/kubernetes/admin.conf or $HOME/.kube/config
# References AWS
https://aws.amazon.com/blogs/compute/kubernetes-clusters-aws-kops/